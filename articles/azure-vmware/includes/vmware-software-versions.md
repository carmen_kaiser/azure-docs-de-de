---
title: Versionen von VMware-Software
description: Unterstützte VMware-Softwareversionen für Azure VMware Solution
ms.topic: include
ms.date: 07/20/2021
ms.openlocfilehash: 722c7e51b567e334bd56593c40645eea8645a491
ms.sourcegitcommit: 7d63ce88bfe8188b1ae70c3d006a29068d066287
ms.translationtype: HT
ms.contentlocale: de-DE
ms.lasthandoff: 07/22/2021
ms.locfileid: "114466376"
---
<!-- Used in faq.md and concepts-private-clouds-clusters#host-maintenance-and-lifecycle-management -->


Die VMware-Softwareversionen in neuen Bereitstellungen von Private Cloud-Clustern von Azure VMware Solution sind wie folgt:

| Software              |    Version   |
| :---                  |     :---:    |
| VCSA/vSphere/ESXi |    6.7 U3l   | 
| ESXi                  |    6.7 U3l   | 
| vSAN                  |    6.7 U3l   |
| HCX                   |    4.1       |
| NSX-T <br />**HINWEIS:** NSX-T ist die einzige unterstützte Version von NSX.               |      [3.1.2](https://docs.vmware.com/en/VMware-NSX-T-Data-Center/3.1/rn/VMware-NSX-T-Data-Center-312-Release-Notes.html)     |


Die aktuell ausgeführte Softwareversion wird auf neue Cluster, die einer vorhandenen privaten Cloud hinzugefügt werden, angewendet. Weitere Informationen finden Sie in den [Anforderungen der VMware-Softwareversion](https://docs.vmware.com/en/VMware-HCX/4.1/hcx-user-guide/GUID-54E5293B-8707-4D29-BFE8-EE63539CC49B.html).

