---
title: Schnellstart für die Sprechererkennung – Speech-Dienst
titleSuffix: Azure Cognitive Services
description: Erfahren Sie, wie Sie mit der Sprechererkennung aus dem Sprach-SDK die Frage „Wer spricht?“ beantworten können. In dieser Schnellstartanleitung erfahren Sie mehr über allgemeine Entwurfsmuster für die Arbeit mit Sprecherüberprüfung und -identifizierung, bei denen beide Sprachbiometrie verwenden, um eindeutige Stimmen zu erkennen.
services: cognitive-services
author: nitinme
manager: nitinme
ms.service: cognitive-services
ms.subservice: speech-service
ms.topic: quickstart
ms.date: 09/02/2020
ms.author: nitinme
ms.custom: devx-track-csharp, cog-serv-seo-aug-2020
zone_pivot_groups: programming-languages-set-twenty-five
keywords: Sprechererkennung, Sprachbiometrie
ms.openlocfilehash: 85c7800bae08ebac11779ad049f2897e0a480b7d
ms.sourcegitcommit: e6de87b42dc320a3a2939bf1249020e5508cba94
ms.translationtype: HT
ms.contentlocale: de-DE
ms.lasthandoff: 07/27/2021
ms.locfileid: "114711548"
---
# <a name="get-started-with-speaker-recognition"></a>Erste Schritte mit der Sprechererkennung

::: zone pivot="programming-language-csharp"
[!INCLUDE [C# Basics include](includes/how-to/speaker-recognition-basics/speaker-recognition-basics-csharp.md)]
::: zone-end

::: zone pivot="programming-language-cpp"
[!INCLUDE [C++ Basics include](includes/how-to/speaker-recognition-basics/speaker-recognition-basics-cpp.md)]
::: zone-end

::: zone pivot="programming-language-javascript"
[!INCLUDE [JavaScript Basics include](includes/how-to/speaker-recognition-basics/speaker-recognition-basics-javascript.md)]
::: zone-end

::: zone pivot="programming-language-curl"
[!INCLUDE [JavaScript Basics include](includes/how-to/speaker-recognition-basics/speaker-recognition-basics-curl.md)]
::: zone-end

## <a name="next-steps"></a>Nächste Schritte

* Ausführliche Informationen zu Klassen und Funktionen finden Sie in der [Referenzdokumentation](/rest/api/speakerrecognition/) zur Sprechererkennung.

* Siehe [C#](https://github.com/Azure-Samples/cognitive-services-speech-sdk/tree/master/quickstart/csharp/dotnet/speaker-recognition)- und [C++](https://github.com/Azure-Samples/cognitive-services-speech-sdk/tree/master/quickstart/cpp/windows/speaker-recognition)-Beispiele auf GitHub.