---
title: Schnellstart für die Spracherkennung – Speech-Dienst
titleSuffix: Azure Cognitive Services
description: Hier erfahren Sie, wie Sie das Speech SDK verwenden, um Sprache in Text zu konvertieren. Dieser Schnellstart enthält Informationen zur Objektkonstruktion, zu unterstützten Audioeingabeformaten und zu Konfigurationsoptionen für die Spracherkennung.
services: cognitive-services
author: nitinme
manager: nitinme
ms.service: cognitive-services
ms.subservice: speech-service
ms.topic: quickstart
ms.date: 09/15/2020
ms.author: nitinme
ms.custom: devx-track-python, devx-track-js, devx-track-csharp, cog-serv-seo-aug-2020
zone_pivot_groups: programming-languages-set-twenty-three
keywords: Spracherkennung, Spracherkennungssoftware
ms.openlocfilehash: 70e19a375fcf20c0d50a4ce2d324391bd60a0807
ms.sourcegitcommit: e6de87b42dc320a3a2939bf1249020e5508cba94
ms.translationtype: HT
ms.contentlocale: de-DE
ms.lasthandoff: 07/27/2021
ms.locfileid: "114711566"
---
# <a name="get-started-with-speech-to-text"></a>Erste Schritte mit der Spracherkennung

::: zone pivot="programming-language-csharp"
[!INCLUDE [C# Basics include](includes/how-to/speech-to-text-basics/speech-to-text-basics-csharp.md)]
::: zone-end

::: zone pivot="programming-language-cpp"
[!INCLUDE [C++ Basics include](includes/how-to/speech-to-text-basics/speech-to-text-basics-cpp.md)]
::: zone-end

::: zone pivot="programming-language-go"
[!INCLUDE [Go include](includes/how-to/speech-to-text-basics/speech-to-text-basics-go.md)]
::: zone-end

::: zone pivot="programming-language-java"
[!INCLUDE [Java Basics include](includes/how-to/speech-to-text-basics/speech-to-text-basics-java.md)]
::: zone-end

::: zone pivot="programming-language-nodejs"
[!INCLUDE [JavaScript Basics include](includes/how-to/speech-to-text-basics/speech-to-text-basics-javascript.md)]
::: zone-end

::: zone pivot="programming-language-browserjs"
[!INCLUDE [JavaScript Basics include](includes/how-to/speech-to-text-basics/speech-to-text-basics-browser-js.md)]
::: zone-end

::: zone pivot="programming-languages-objectivec-swift"
[!INCLUDE [ObjectiveC/Swift Basics include](includes/how-to/speech-to-text-basics/speech-to-text-basics-objectivec-swift.md)]
::: zone-end

::: zone pivot="programming-language-python"
[!INCLUDE [Python Basics include](./includes/how-to/speech-to-text-basics/speech-to-text-basics-python.md)]
::: zone-end

::: zone pivot="programming-language-curl"
[!INCLUDE [REST include](includes/how-to/speech-to-text-basics/speech-to-text-basics-curl.md)]
::: zone-end

::: zone pivot="programmer-tool-spx"
[!INCLUDE [CLI include](includes/how-to/speech-to-text-basics/speech-to-text-basics-cli.md)]
::: zone-end

## <a name="next-steps"></a>Nächste Schritte

* [Verwenden von per Codec komprimierter Audioeingabe mit dem Speech SDK](how-to-use-codec-compressed-audio-input-streams.md)
* Sehen Sie sich die [Schnellstartbeispiele](https://github.com/Azure-Samples/cognitive-services-speech-sdk/tree/master/quickstart) auf GitHub an.
