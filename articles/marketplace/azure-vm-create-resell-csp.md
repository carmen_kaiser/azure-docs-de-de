---
title: Verkaufen Ihres Angebots über Cloud Solution Provider (CSP) im Azure Marketplace
description: Verkaufen Ihres Angebots über Cloud Solution Provider (CSP) im Azure Marketplace
ms.service: marketplace
ms.subservice: partnercenter-marketplace-publisher
ms.topic: how-to
author: mingshen-ms
ms.author: mingshen
ms.date: 11/05/2020
ms.openlocfilehash: fd02ae6a9c58979c9d4747e67948df69748f3f1a
ms.sourcegitcommit: 70ce9237435df04b03dd0f739f23d34930059fef
ms.translationtype: HT
ms.contentlocale: de-DE
ms.lasthandoff: 06/05/2021
ms.locfileid: "111525064"
---
# <a name="resell-your-offer-through-csp"></a>Verkaufen Ihres Angebots über CSP

## <a name="resell-through-csp"></a>Verkauf über CSP

Auf der Seite **Verkauf über CSPs** (Option in Partner Center im linken Navigationsmenü) können Sie die Reichweite Ihres Angebots vergrößern, indem Sie es für Partner im [Cloud Solution Provider-Programm (CSP)](https://azure.microsoft.com/offers/ms-azr-0145p/) verfügbar machen. Dadurch können Handelspartner Ihr Angebot an ihre Kunden verkaufen und gebündelte Lösungen erstellen. Alle BYOL-Pläne (Bring-Your-Own-License) werden im Programm automatisch aktiviert. Sie können auch Ihre Nicht-BYOL-Pläne aktivieren.

Wählen Sie aus, welche Partner in der Lage sein sollen, Ihr Angebot zu verkaufen:

- Alle Partner im CSP-Programm
- Bestimmte, von mir ausgewählte Partner im CSP-Programm
- Keine Partner im CSP-Programm

Wählen Sie **Entwurf speichern** aus, bevor Sie mit der nächsten Registerkarte im linken Navigationsmenü (**Testversion**) fortfahren.

## <a name="next-steps"></a>Nächste Schritte

- [Konfigurieren einer Testversion](azure-resource-manager-test-drive.md)
- Nach dem Einrichten einer Testversion (optional) [überprüfen und veröffentlichen Sie das Angebot](review-publish-offer.md).
