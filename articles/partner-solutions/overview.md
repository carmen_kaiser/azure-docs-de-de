---
title: 'Angebote von Partnern: Azure-Partnerlösungen'
description: Erfahren Sie mehr über Lösungen, die von Partnern in Azure angeboten werden.
author: tfitzmac
ms.topic: conceptual
ms.service: partner-services
ms.date: 05/25/2021
ms.author: tomfitz
ms.openlocfilehash: 208f8d4e8d0fc8b688f45af5e8164b41c3eba768
ms.sourcegitcommit: 80d311abffb2d9a457333bcca898dfae830ea1b4
ms.translationtype: HT
ms.contentlocale: de-DE
ms.lasthandoff: 05/26/2021
ms.locfileid: "110464092"
---
# <a name="extend-azure-with-solutions-from-partners"></a>Erweitern von Azure mit Lösungen von Partnern

Partnerorganisationen bieten Lösungen an, die Sie in Azure zur Erweiterung Ihrer Cloudinfrastruktur verwenden können. Diese Lösungen sind vollständig in Azure integriert. Sie nutzen diese Lösungen fast auf dieselbe Weise wie Lösungen von Microsoft. Sie verwenden einen Ressourcenanbieter, Ressourcentypen und SDKs, um die Lösung zu verwalten.

Partnerlösungen sind über den Marketplace verfügbar.

| Partnerlösung | Beschreibung |
| :--- | :--- |
| [Apache Kafka für Confluent Cloud](./apache-kafka-confluent-cloud/overview.md) | Vollständig verwaltete Ereignisstreaming-Plattform unterstützt von Apache Kafka |
| [Datadog](./datadog/overview.md) | Überwachen Sie Ihre Server, Clouds, Metriken und Apps an zentraler Stelle. |
| [Elastic](./elastic/overview.md) | Überwachen Sie die Integrität und Leistung Ihrer Azure-Umgebung. |
