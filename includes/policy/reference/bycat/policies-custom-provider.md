---
author: DCtheGeek
ms.service: azure-policy
ms.topic: include
ms.date: 07/16/2021
ms.author: dacoulte
ms.custom: generated
ms.openlocfilehash: 410278ee5138baf3ccf56b17a93b9da141153530
ms.sourcegitcommit: e2fa73b682a30048907e2acb5c890495ad397bd3
ms.translationtype: HT
ms.contentlocale: de-DE
ms.lasthandoff: 07/16/2021
ms.locfileid: "114386183"
---
|Name<br /><sub>(Azure-Portal)</sub> |Beschreibung |Auswirkungen |Version<br /><sub>(GitHub)</sub> |
|---|---|---|---|
|[Zuordnungen für einen benutzerdefinierten Anbieter bereitstellen](https://portal.azure.com/#blade/Microsoft_Azure_Policy/PolicyDetailBlade/definitionId/%2Fproviders%2FMicrosoft.Authorization%2FpolicyDefinitions%2Fc15c281f-ea5c-44cd-90b8-fc3c14d13f0c) |Hiermit wird eine Zuordnungsressource bereitgestellt, die dem angegebenen benutzerdefinierten Anbieter ausgewählte Ressourcentypen zuordnet. Diese Richtlinienbereitstellung unterstützt keine geschachtelten Ressourcentypen. |deployIfNotExists |[1.0.0](https://github.com/Azure/azure-policy/blob/master/built-in-policies/policyDefinitions/Custom%20Provider/AssociationForCustomProvider_Deploy.json) |
